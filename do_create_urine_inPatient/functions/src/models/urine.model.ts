export interface UrineModel {
    id?: number|string;
    volumeUrine:string;
    volume_TotalUrine:string;
    date:string;
    hour:string;
}