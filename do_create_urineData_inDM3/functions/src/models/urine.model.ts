export interface UrineModel{
    id?: number|string;
    "volumeUrine(mL)":string;
    "volume_TotalUrine(mL)":string;
    date:string;
    hour:string;
}