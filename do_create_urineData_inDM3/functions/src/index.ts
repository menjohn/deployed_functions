import * as admin from "firebase-admin";
import {firestore} from "firebase-admin/lib/firestore";
import * as functions from "firebase-functions";
import DocumentReference = firestore.DocumentReference;
import {DocumentSnapshot} from "firebase-functions/lib/providers/firestore";
import FieldValue = firestore.FieldValue;
import * as bodyParser from "body-parser";
import * as express from 'express';
import * as cors from 'cors';
import {cert} from "./cred/cert";
import {UrineModel} from "./models/urine.model";
import {PatientModel} from "./models/patient.model";

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://visaal-fc520.firebaseio.com"
});

const db = admin.firestore();
const refPatients=db.collection('usersDM3')
const refUrines=db.collection('urinesDM')
const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(cors());

export async function do_create_urine_inPatient(newUrine:UrineModel,patientId:string): Promise<UrineModel>{
    if(!patientId || !newUrine){
        throw new Error('patient id and newUrine must be filled');
    }
    const patientRef=refPatients.doc(patientId);
    const patientSnap:DocumentSnapshot=await patientRef.get();
    const patientData: PatientModel=patientSnap.data() as PatientModel;
    if(!patientSnap.exists){
        throw new Error('patient dont exist');
    }
    const createdUrineRef:DocumentReference= await refUrines.add(newUrine);
    const createNewUrine:DocumentReference=refUrines.doc(createdUrineRef.id);
    await createNewUrine.set({...newUrine,id:createNewUrine.id});
    const urineInPatient:DocumentReference=patientRef
        .collection('urinesDM').doc(createdUrineRef.id);
    await urineInPatient.set({ref:createdUrineRef,uid:createdUrineRef.id});

    const urinesId:any[]=[];  //on crée un tableau qui va contenir les id de toutes les urines
    urinesId.push(createdUrineRef.id);
    patientData.urinesDM=urinesId;
    await patientRef.update({'urinesDM': FieldValue.arrayUnion(createdUrineRef.id)});
    await urineInPatient.set({ref: urineInPatient, id: urineInPatient.id, ...newUrine}, {merge: true});
    return {...newUrine,id:createdUrineRef.id};
}

app.post('/:patientId/',async (req,res)=>{
    try{
        const patientId:string=req.params.patientId;
        const newUrine:UrineModel=req.body;
        const addResult = await do_create_urine_inPatient(newUrine,patientId);
        return  res.send(addResult);
    } catch(e){
        return res.status(500).send({error:'error post urine :'+e.message});
    }
})

export const do_create_UrineData_inDM3 = functions
    .region('europe-west1')
    .runWith({memory:"256MB"})
    .https.onRequest(app)