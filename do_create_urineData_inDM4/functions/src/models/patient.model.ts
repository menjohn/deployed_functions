import {UrineModel} from "./urine.model";

export interface PatientModel{
    id?: number|string;
    firstName: string;
    lastName: string;
    birthdate:string;
    floor:number;
    bedNumber:number;
    roomNumber:number;
    service:string;
    sex: string;
    deviceNumber:string;
    urinesDM?: UrineModel[];
}