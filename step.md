1st step use : 
 ``firebase init`` on terminal in the function directory 

2: say yes for to proceed

3: choose Firestore, Functions, Hosting (firebase) and Emulator

4: Choose "Use an existing project" and select the project of your choice

5: you can let in default firestore rules and indexes

6: choose Typescript

7: Say no to ESLint

8: Install the dependencies 

9: create the directory functions/lib

10: say no to configure as single page app and automatic build with github

11: Re Check Functions Firestore and Hosting

12: Set by default for the emulators

13: say yes for enable Emulator Ui

14: no need to choose Emulator Ui port

15: Download the emulator 

#16: In package.Json file use these 3 lines in "scripts":
`"watch": "tsc -w",`
`"serve": "npm run build && firebase serve",`
`"emulate": "npm run build && firebase emulators:start --only functions",`

#17 : Rename the deploy like this 
```"deploy:test": "firebase deploy --only functions:test"```

#18 : add cred/cert in the src folder 

#18 install express, body parser and cors in functions
```npm install express```
```npm install body-parser```
``npm install cors`` or
``npm i -D @types/cors`` instead of the previous line if it doesn't work


#19 to change project you can use 
``firebase use <your project>``