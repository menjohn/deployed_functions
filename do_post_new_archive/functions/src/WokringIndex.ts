import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import * as bodyParser from "body-parser";
import * as express from 'express';
import * as cors from 'cors';
import {cert} from "./cred/cert";
import {ArchiveModel} from "./models/archive.model";
import * as uuid from "uuidv4";
//uuid est nécessaire pour pouvoir attribuer une clé u fichier qui sera envoyé et donc de le consulter

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://visaal-fc520.firebaseio.com",
    storageBucket: "visaal-fc520.appspot.com"
});

const db = admin.firestore();
const refArchive=db.collection('archivesTest')
const bucket = admin.storage().bucket();
const app = express();
//const key = 'TIbv/fjexq+VmtXzAlc63J4z5kFmWJ6NdAPQulQBT7g=';
app.use(cors());
app.use(bodyParser.json());
app.use(cors());

export async function do_create_archive(newArchive:ArchiveModel):Promise<ArchiveModel>{
    if(!newArchive){
        throw new Error('new archive must be filled');
    }
    await refArchive.add(newArchive);
    return{...newArchive};
}

app.post('/',async (req,res)=>{
    try{
        const archive = req.body;
        const patientDataFile = await do_create_archive(archive);
        const firstName= patientDataFile.firstName;
        const lastName= patientDataFile.lastName.toUpperCase();
        const filePath= patientDataFile.fichierPdf;
        await bucket.upload(filePath,{
            destination:`filesDataPatients/${lastName}_${firstName}.pdf`,
            metadata: {
                contentType:'application/pdf',
                //encryptionKey:Buffer.from(key,'base64'),
                metadata: {
                    firebaseStorageDownloadTokens : uuid
                }
            }
        });
        //await patientDataFile.fichierPdf = "recupérer l'URL" ;
        return res.send(patientDataFile);
    } catch (e) {
        return res.status(500).send({error:'error post :'+e.message});
    }
});

export const do_create_Archive = functions
    .region('europe-west1')
    .https.onRequest(app)