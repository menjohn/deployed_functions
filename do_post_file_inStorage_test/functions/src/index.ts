import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {cert} from "./cred/cert";
import * as bodyParser from "body-parser";
import * as express from 'express';
import * as cors from 'cors';

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://visaal-fc520.firebaseio.com",
    storageBucket: "visaal-fc520.appspot.com",
});

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(cors());

const storageBucket = admin.storage().bucket();
export async function do_post_inStorage(){
    await storageBucket.upload(
        'C:/Users/dymen/Desktop/VIsaal/deployTest/backend/do_post_test/newPDF(14).pdf',
        {
            destination : '/InfoPatientDM3.pdf',
            //metadata: {contentType:'application/pdf'}
        }
    )
}

app.post('/',async (req,res)=>{
    try{
        return await do_post_inStorage();
        return res.send(`Fiche patient enrgistré`);
    } catch(e){
        return res.status(500).send({error:'error post file :'+e.message});
    }
})

export const do_Post_inStorage =functions
    .region('europe-west1')
    .https.onRequest(app)