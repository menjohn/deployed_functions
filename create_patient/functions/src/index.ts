import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {PatientModel} from "./models/patient.model";
import {cert} from "./cred/cert";
import * as bodyParser from "body-parser";
import * as express from 'express';
import * as cors from 'cors';
import {firestore} from "firebase-admin/lib/firestore";
import DocumentData = firestore.DocumentData;
import DocumentReference = firestore.DocumentReference;

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://visaal-fc520.firebaseio.com"
})

const db = admin.firestore();
const refPatients=db.collection('patients');
const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(cors());


export async function do_create_patient(newPatient :PatientModel): Promise<PatientModel>{
    if(!newPatient){
        throw new Error('new patient must be filled');
    }
    const addResult: DocumentReference<DocumentData>= await refPatients.add(newPatient);
    await addResult.update({id:addResult.id});

    return{...newPatient,id:addResult.id};
}

app.post('/',async (req,res)=>{
    try{
        const patient = req.body;
        const sendPatient=await do_create_patient(patient);
        return res.send(sendPatient);
    } catch(e){
        return res.status(500).send({error:'erreur post :'+e.message});
    }
});

export const do_create_Patient = functions.https.onRequest(app);