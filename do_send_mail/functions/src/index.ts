import * as admin from "firebase-admin";
import {firestore} from "firebase-admin/lib/firestore";
import DocumentReference = firestore.DocumentReference;
import * as functions from "firebase-functions";
import {DocumentSnapshot} from "firebase-functions/lib/providers/firestore";
import * as express from 'express';
import * as nodemailer from 'nodemailer';
import {cert} from "./cred/cert";
import {PatientModel} from "./models/patient.model";


admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://visaal-fc520.firebaseio.com",
    storageBucket: "visaal-fc520.appspot.com/filesDataPatients"
});

const transport = nodemailer.createTransport({
    host: "smtp.mailtrap.io",
    port: 2525,
    auth: {
        user: "32de1faa495d79",
        pass: "847c06f85ba667"
    }
});

const db = admin.firestore();
const refPatients=db.collection('usersDM3')
const app = express();

export async function do_send_mail_from_DM3(patientId:string):Promise<PatientModel>{
    //Première partie pour récupérer les informations du patient à changer plus tard par récupération du fichier PDF
    if(!patientId){
        throw new Error('patientId must be filled')
    }
    const patientRef:DocumentReference=refPatients.doc(patientId);
    const patientSnap:DocumentSnapshot=await patientRef.get();
    if (!patientSnap.exists){
        throw new Error('patient does not exist')
    }
    else{
        return patientSnap.data() as PatientModel;
    }

};

app.get('/:patientId/', async (req,res)=>{
    try{
        const patientId:string=req.params.patientId;
        const patientData = await do_send_mail_from_DM3(patientId);
        const lastName = patientData.lastName.toUpperCase();
        const firstsName= patientData.firstName;

        const mailOptions={
            from:"dispositifVisaal@visaal-MSsanté.com",
            to: req.body.email,
            subject:`rapport d'utilisation du dispositif Visaal ${lastName} ${firstsName}` ,
            text: `Ci-joint les informations concernant le patient ${lastName} ${firstsName} après utilisation du dispositif Visaal.`,
            attachments:[
                {
                    filename:'infoPatient.txt',
                    content:`Hello ${JSON.stringify(patientData)}`,
                },
            ],//cette partie sera à changer afin qu'on y incropore le PDF voire le dossier du patient selon la norme
        };
        return transport.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            return res.send(`Mail sent to : ${req.body.email}`);
        });
    } catch (e) {
        return res.status(500).send({error:'error post mail :'+e.message});
    }
});

export const do_Send_Mail_from_DM3 =functions
    .region('europe-west1')
    .https.onRequest(app)
